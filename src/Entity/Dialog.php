<?php

namespace Drupal\guide\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Dialog entity.
 *
 * @ingroup guide
 *
 * @ContentEntityType(
 *   id = "dialog",
 *   label = @Translation("Dialog"),
 *   handlers = {
 *     "storage" = "Drupal\guide\DialogStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\guide\DialogListBuilder",
 *     "views_data" = "Drupal\guide\Entity\DialogViewsData",
 *     "translation" = "Drupal\guide\DialogTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\guide\Form\DialogForm",
 *       "add" = "Drupal\guide\Form\DialogForm",
 *       "edit" = "Drupal\guide\Form\DialogForm",
 *       "delete" = "Drupal\guide\Form\DialogDeleteForm",
 *     },
 *     "access" = "Drupal\guide\DialogAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\guide\DialogHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "dialog",
 *   data_table = "dialog_field_data",
 *   revision_table = "dialog_revision",
 *   revision_data_table = "dialog_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   translatable = TRUE,
 *   admin_permission = "administer dialog entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/guide/dialog/{dialog}",
 *     "add-form" = "/admin/structure/guide/dialog/add",
 *     "edit-form" = "/admin/structure/guide/dialog/{dialog}/edit",
 *     "delete-form" = "/admin/structure/guide/dialog/{dialog}/delete",
 *     "version-history" = "/admin/structure/guide/dialog/{dialog}/revisions",
 *     "revision" = "/admin/structure/guide/dialog/{dialog}/revisions/{dialog_revision}/view",
 *     "revision_revert" = "/admin/structure/guide/dialog/{dialog}/revisions/{dialog_revision}/revert",
 *     "revision_delete" = "/admin/structure/guide/dialog/{dialog}/revisions/{dialog_revision}/delete",
 *     "translation_revert" = "/admin/structure/guide/dialog/{dialog}/revisions/{dialog_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/guide/dialog",
 *   },
 *   field_ui_base_route = "dialog.settings"
 * )
 */
class Dialog extends RevisionableContentEntityBase implements DialogInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the dialog owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   *
   */
  public static function getFacetSources() {
    /** @var \Drupal\facets\FacetManager\DefaultFacetManager $facet_manager */
    $facet_manager = \Drupal::service('facets.manager');

    $enabled_facets = $facet_manager->getEnabledFacets();
    $sources = [];
    foreach ($enabled_facets as $facet) {
      $facet_source_id = $facet->getFacetSourceId();
      // TODO: Does a facet source have a (human readable) name?
      $sources[$facet_source_id] = $facet_source_id;
    }
    return $sources;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Dialog entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Introduction'))
      ->setDescription(t('An introductory text to be shown to the user.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'settings' => array(
          'display_label' => TRUE,
        ),
        'weight' => -8,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['target'] = BaseFieldDefinition::create('list_string')
      ->setSettings([
        'allowed_values' => Dialog::getFacetSources(),
      ])
      ->setLabel(t('Target search'))
      ->setDescription(t('Select the target search for which the questions can provide parameters.'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -7
      ]);

    $fields['questions'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Questions'))
      ->setDescription(t('The questions used in this dialog.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'question')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -6,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('A boolean indicating whether the dialog is enabled.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -5,
      ]);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Question entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['conditions'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Conditions'))
      ->setDescription(t('The conditional logic for the question.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    return $fields;
  }

}
