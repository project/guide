<?php

namespace Drupal\guide\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Dialog entities.
 *
 * @ingroup guide
 */
interface DialogInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Dialog name.
   *
   * @return string
   *   Name of the Dialog.
   */
  public function getName();

  /**
   * Sets the Dialog name.
   *
   * @param string $name
   *   The Dialog name.
   *
   * @return \Drupal\guide\Entity\DialogInterface
   *   The called Dialog entity.
   */
  public function setName($name);

  /**
   * Gets the Dialog creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Dialog.
   */
  public function getCreatedTime();

  /**
   * Sets the Dialog creation timestamp.
   *
   * @param int $timestamp
   *   The Dialog creation timestamp.
   *
   * @return \Drupal\guide\Entity\DialogInterface
   *   The called Dialog entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Dialog published status indicator.
   *
   * Unpublished Dialog are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Dialog is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Dialog.
   *
   * @param bool $published
   *   TRUE to set this Dialog to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\guide\Entity\DialogInterface
   *   The called Dialog entity.
   */
  public function setPublished($published);

  /**
   * Gets the Dialog revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Dialog revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\guide\Entity\DialogInterface
   *   The called Dialog entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Dialog revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Dialog revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\guide\Entity\DialogInterface
   *   The called Dialog entity.
   */
  public function setRevisionUserId($uid);

}
