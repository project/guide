<?php

namespace Drupal\guide;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Defines an interface for the link field item.
 */
interface AnswerItemInterface extends FieldItemInterface {

  /**
   * Gets the Answer key.
   *
   * @return int
   *   Returns an integer.
   */
  public function getKey();

  /**
   * Gets the Action object.
   *
   * @return \Drupal\guide\Action
   *   Returns an Action object.
   */
  public function getAction();

}
