<?php

namespace Drupal\guide\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\guide\AnswerItemInterface;

/**
 * Plugin implementation of the 'answer_field' field type.
 *
 * @FieldType(
 *   id = "answer_field",
 *   label = @Translation("Answer"),
 *   description = @Translation("An answer with an action attached."),
 *   default_widget = "answer_widget",
 *   default_formatter = "answer_formatter",
 *   cardinality = -1,
 * )
 */
class AnswerField extends FieldItemBase implements AnswerItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 255,
      'is_ascii' => FALSE,
      'case_sensitive' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Answer key'))
      #->setRequired(TRUE)
    ;

    $properties['text'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Answer text'))
      #->setRequired(TRUE)
    ;

    $properties['action'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Action'))
      #->setRequired(TRUE)
    ;

    $properties['options'] = MapDataDefinition::create()
      ->setLabel(t('Options'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'value' => [
        'description' => 'Answer key',
        'type' => 'int',
        'unsigned' => TRUE,
      ],
      'text' => [
        'description' => 'Answer text',
        'type' => 'varchar',
        'length' => 255,
      ],
      'action' => [
        'description' => 'Action key',
        'type' => 'int',
        'unsigned' => TRUE,
      ],
      'options' => [
        'description' => 'Serialized array of options for the link.',
        'type' => 'blob',
        'size' => 'big',
        'serialize' => TRUE,
      ],
    ];

    $schema = [
      'columns' => $columns,
      'indexes' => [],
      'foreign keys' => [],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    if ($max_length = $this->getSetting('max_length')) {
      $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
      $constraints[] = $constraint_manager->create('ComplexData', [
        'value' => [
          'Length' => [
            'max' => $max_length,
            'maxMessage' => t('%name: may not be longer than @max characters.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max' => $max_length
            ]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['value'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['max_length'] = [
      '#type' => 'number',
      '#title' => t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    $value = $this->get('value')->getValue();
    return $value;
  }



  /**
   * {@inheritdoc}
   */
  public function getAction() {
    $value = $this->get('action')->getValue();
    return $value;
  }

}
