<?php

namespace Drupal\guide\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\DataType\FieldItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'answer_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "answer_formatter",
 *   label = @Translation("Answer formatter"),
 *   field_types = {
 *     "answer_field"
 *   }
 * )
 */
class AnswerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $i = 0;
    /**
     * @var int $delta
     * @var FieldItemInterface $item
     */
    foreach ($items as $delta => $item) {
      $markup = '';
      if ($i == 0) {
        $markup .= '<ul>';
      }
      $markup .= '<li>' . $item->getValue()['text'] . '</li>';
      if ($i == $items->count() - 1) {
        $markup .= '</ul>';
      }

      $elements[$delta] = ['#markup' => $markup];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
