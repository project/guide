<?php

namespace Drupal\guide\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\guide\AnswerItemInterface;
use \Drupal\facets\FacetManager\DefaultFacetManager;

/**
 * Plugin implementation of the 'answer_widget' widget.
 *
 * @FieldWidget(
 *   id = "answer_widget",
 *   label = @Translation("Answer widget"),
 *   field_types = {
 *     "answer_field"
 *   },
 * )
 */
class AnswerWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];


    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $answer_keys = [];
    /** @var AnswerItemInterface $item */
    foreach ($items as $item) {
      $answer_keys[] = $item->getKey();
    }

    $form['#prefix'] = '<div id="question-ajax-wrapper">';
    $form['#suffix'] = '</div>';
    $form['#tree'] = TRUE;
    $values = $form_state->getValues();

    #\Drupal::logger('guide')->notice(print_r(array_keys($values['answers']), 1));

    $element['value'] = [
      '#type' => 'hidden',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : max($answer_keys) + 1 + $delta,
    ];

    $element['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Answer text'),
      '#default_value' => isset($items[$delta]->text) ? $items[$delta]->text : NULL,
    ];

    $element['ajax_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ajax_wrapper'],
    ];
    $element['ajax_wrapper']['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#default_value' => isset($items[$delta]->action) ? $items[$delta]->action : NULL,
      '#options' => [
        0 => t('None'),
        1 => t('Add filter'),
        2 => t('Redirect to node'),
      ],
      '#ajax' => [
        'callback' => [$this, 'changeAction'],
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
        'wrapper' => 'question-ajax-wrapper',
      ],
    ];

    if (isset($values['answers'][$delta]['ajax_wrapper']['action'])) {
      $action = $values['answers'][$delta]['ajax_wrapper']['action'];
    }
    else if (isset($items[$delta]->action)) {
      $action = $items[$delta]->action;
    }
    else {
      $action = NULL;
    }

    $options = $items[$delta]->options;

//    if (isset($items[$delta])) {
//      $element['ajax_wrapper']['action_delta'] = [
//        '#type' => 'markup',
//        '#markup' => "<pre>Delta is " . $delta . "</pre>",
//      ];
//    }
    if (!is_null($action)) {
      $element['ajax_wrapper']['action_debug'] = [
        '#type' => 'markup',
        '#markup' => "<pre>" . time() . "<br>Item Action is " . $items[$delta]->action . "<br>FormState Action is " . $action . "<br>Options are " . print_r($options, 1) . "</pre>",
      ];
    }



    if ($action > 0) {
      $element['ajax_wrapper']['options'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Action configuration'),
        '#attributes' => [
          'id' => ['edit-filter'],
        ],
//        '#states' => [
//          'invisible' => [
//            'select[name="answers[' . $delta . '][ajax_wrapper][action]"]' => [
//              'value' => '0',
//            ],
//          ],
//        ],
      ];

      /* ### FILTER ### */
      if ($action == 1) {

        /** @var \Drupal\facets\FacetManager\DefaultFacetManager $facet_manager */
        $facet_manager = \Drupal::service('facets.manager');

        $enabled_facets = $facet_manager->getEnabledFacets();
        $facets = [];
        $sources = [];
        foreach ($enabled_facets as $facet) {
          #$facet_source = $facet->getFacetSource();
          $facet_source_id = $facet->getFacetSourceId();
          $id_parts = explode('__', $facet_source_id);
          $human_readable = strtoupper(str_replace('_', ' ', $id_parts[1]));
          $facet_source_config_id = $facet->getFacetSourceConfig()->id();
          #\Drupal::logger('guide')->notice(print_r($facet_source->getPath(), 1));
          $sources[$facet_source_id] = $human_readable;
          $facets[$facet_source_id][$facet->id()] = $facet->label();
        }

        if (isset($values['answers'][$delta]['ajax_wrapper']['options']['source'])) {
          $selected_source = $values['answers'][$delta]['ajax_wrapper']['options']['source'];
        }
        else if (isset($options['source'])) {
          $selected_source = $options['source'];
        }
        else {
          $selected_source = NULL;
        }

        $element['ajax_wrapper']['options']['source'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose facet source'),
          '#options' => $sources,
          '#default_value' => $selected_source,
          '#ajax' => [
            'callback' => [$this, 'changeAction'],
            'event' => 'change',
            'progress' => [
              'type' => 'throbber',
            ],
            'wrapper' => 'question-ajax-wrapper',
          ],
//          '#states' => [
//            'visible' => [
//              'select[name="answers['. $delta . '][ajax_wrapper][action]"]' => [
//                'value' => '1',
//              ],
//            ],
//          ],
        ];

        if ($selected_source) {
          $element['ajax_wrapper']['options']['facets'] = [
            '#type' => 'checkboxes',
            '#title' => $this->t('Choose facets'),
            '#options' => $facets[$selected_source],
            '#default_value' => isset($options['facets']) ? array_keys($options['facets']) : NULL,
            '#ajax' => [
              'callback' => [$this, 'changeAction'],
              'event' => 'change',
              'progress' => [
                'type' => 'throbber',
              ],
              'wrapper' => 'question-ajax-wrapper',
            ],
//            '#states' => [
//              'visible' => [
//                'select[name="answers[' . $delta . '][ajax_wrapper][options]"]' => [
//                  'value' => '1',
//                ],
//              ],
//            ],
          ];

          if (isset($values['answers'][$delta]['ajax_wrapper']['options']['facets'])) {
            $selected_facets = array_filter($values['answers'][$delta]['ajax_wrapper']['options']['facets']);
          }
          else if (isset($options['facets'])) {
            $selected_facets = array_filter($options['facets']);
          }
          else {
            $selected_facets = NULL;
          }

//          $element['ajax_wrapper']['options']['selected_facets'] = [
//            '#type' => 'markup',
//            '#markup' => "<pre>Selected Source: " . print_r($selected_source, 1) . "</pre>" . "<pre>Available Facets: " . print_r($facets[$selected_source], 1) . "</pre>" . "<pre>Selected Facets: " . print_r($selected_facets, 1) . "</pre>",
//          ];

          if ($selected_facets) {
            foreach ($selected_facets as $facetid) {
              $facet = null;
              foreach($enabled_facets as $f) {
                if ($facetid == $f->id()) {
                  $facet = $f;
                  break;
                }
              }
              if ($facet) {
                $element['ajax_wrapper']['options']['facet-values']['facet-' . $facet->id()] = [
                  '#type' => 'fieldset',
                  '#title' => $this->t('Facet configuration') . ': ' . $facet->label(),
                  '#attributes' => [
                    'class' => ["facet-options"],
                  ],
//                '#states' => [
//                  'visible' => [
//                    ':input[name="answers[' . $delta . '][options][facets][' . $facet->id() . ']"]' => [
//                      'checked' => TRUE,
//                    ],
//                  ],
//                ],
                ];


                $processed_facets = [];
                $facet->setWidget('array');
                $processed_facets[] = $facet_manager->build($facet);

                $facet_options = [];
                $data_settings = $facet->getDataDefinition()->getSettings();
                if ($data_settings['target_type'] == 'node_type') {
                  $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
                  foreach ($contentTypes as $contentType) {
                    $facet_options[$contentType->id()] = $contentType->label();
                  }
                }
                else if ($data_settings['target_type'] == 'taxonomy_term') {
                  $vocabularies = [];
                  foreach ($data_settings['handler_settings']['target_bundles'] as $vid) {
                    $vocabularies[] = $vid;
                  }
                  foreach ($vocabularies as $vid) {
                    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                    $i = 0;
                    foreach ($terms as $term) {
                      if ($i == 0) {
                        $facet_options[$term->tid] = $term->name . ' (' . $vid . ')';
                      } else {
                        $facet_options[$term->tid] = $term->name;
                      }
                      $i++;
                    }
                  }
                }

                $element['ajax_wrapper']['options']['facet-values']['facet-' . $facet->id()]['field'] = [
                  '#type' => 'checkboxes',
                  '#title' => $this->t('Choose options to associate to answer'),
                  '#options' => $facet_options,
                  '#default_value' => isset($options['values']['facet-' . $facet->id()]) ? array_keys($options['values']['facet-' . $facet->id()]) : NULL,
                ];
              }
            }
          }
        }


      }
      else if ($action == 2) {
        $element['ajax_wrapper']['options']['redirect'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Node ID'),
          '#default_value' => isset($options['redirect']) ? $options['redirect'] : NULL,
          '#attributes' => [
            'id' => ['edit-redirect'],
          ],
//          '#states' => [
//            'visible' => [
//              'select[name="answers['. $delta . '][ajax_wrapper][action]"]' => [
//                'value' => '2',
//              ],
//            ],
//          ],
          '#description' => $this->t('Enter the node id the user should be redirected to.'),
        ];
        
      }
    }
    return $element;
  }

  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {
//    $redirect = $element['ajax_wrapper']['options']['redirect']['#value'];
//    $node = \Drupal\node\Entity\Node::load($redirect);
//    if (!(isset($node) && $node->isPublished())) {
//      $form_state->setError($element['ajax_wrapper']['options']['redirect'], t("There is no published content with that ID."));
//    }
    #$form_state->setValueForElement($element, '');
    #return;

  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    \Drupal::logger('guide')->notice(print_r($values, 1));
    foreach ($values as $id => &$value) {

      if (empty(trim($value['text']))) {
        unset($values[$id]);
      }
      else {
        $value['action'] = $value['ajax_wrapper']['action'];
        $options = $value['ajax_wrapper']['options'];
        if ($value['ajax_wrapper']['action'] == 2) {
          $value['options'] = [
            'redirect' => $options['redirect'],
          ];
        }
        elseif ($value['ajax_wrapper']['action'] == 1) {
          $facet_values_all = $options['facet-values'];
          $facet_values = [];
          foreach ($facet_values_all as $facetid => $facetdata) {
            $facet_values[$facetid] = array_filter($facetdata['field']);
          }
          $value['options'] = [
            'source' => $options['source'],
            'facets' => array_filter($options['facets']),
            'values' => $facet_values,
          ];
          #$value['options'] = serialize($value['options']);
          //var_dump(['---', $value]);die();
        }
      }
    }

    return parent::massageFormValues($values, $form, $form_state); // TODO: Change the autogenerated stub
  }

  public function changeAction(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    return $form;
  }

}
