<?php

namespace Drupal\guide;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for dialog.
 */
class DialogTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
