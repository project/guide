<?php

namespace Drupal\guide\Controller;

use Drupal\Core\Link;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\guide\Entity\DialogInterface;

/**
 * Class DialogController.
 *
 *  Returns responses for Dialog routes.
 */
class DialogController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Dialog revision.
   *
   * @param int $dialog_revision
   *   The Dialog  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($dialog_revision) {
    $dialog = \Drupal::service('entity_type.manager')->getStorage('dialog')->loadRevision($dialog_revision);
    $view_builder = \Drupal::service('entity_type.manager')->getViewBuilder('dialog');

    return $view_builder->view($dialog);
  }

  /**
   * Page title callback for a Dialog revision.
   *
   * @param int $dialog_revision
   *   The Dialog  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($dialog_revision) {
    $dialog = \Drupal::service('entity_type.manager')->getStorage('dialog')->loadRevision($dialog_revision);
    return $this->t('Revision of %title from %date', ['%title' => $dialog->label(), '%date' => \Drupal::service('date.formatter')->format($dialog->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Dialog.
   *
   * @param \Drupal\guide\Entity\DialogInterface $dialog
   *   A Dialog  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(DialogInterface $dialog) {
    $account = $this->currentUser();
    $langcode = $dialog->language()->getId();
    $langname = $dialog->language()->getName();
    $languages = $dialog->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $dialog_storage = \Drupal::service('entity_type.manager')->getStorage('dialog');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $dialog->label()]) : $this->t('Revisions for %title', ['%title' => $dialog->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all dialog revisions") || $account->hasPermission('administer dialog entities')));
    $delete_permission = (($account->hasPermission("delete all dialog revisions") || $account->hasPermission('administer dialog entities')));

    $rows = [];

    $vids = $dialog_storage->revisionIds($dialog);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\guide\DialogInterface $revision */
      $revision = $dialog_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $dialog->getRevisionId()) {
          // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
          // Please manually remove the `use LinkGeneratorTrait;` statement from this class.
          $link = Link::fromTextAndUrl($date, new Url('entity.dialog.revision', ['dialog' => $dialog->id(), 'dialog_revision' => $vid]));
        }
        else {
          // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
          // Please confirm that `$dialog` is an instance of `\Drupal\Core\Entity\EntityInterface`. Only the method name and not the class name was checked for this replacement, so this may be a false positive.
          $link = $dialog->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.dialog.translation_revert', ['dialog' => $dialog->id(), 'dialog_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.dialog.revision_revert', ['dialog' => $dialog->id(), 'dialog_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.dialog.revision_delete', ['dialog' => $dialog->id(), 'dialog_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['dialog_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
