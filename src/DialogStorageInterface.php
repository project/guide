<?php

namespace Drupal\guide;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\guide\Entity\DialogInterface;

/**
 * Defines the storage handler class for Dialog entities.
 *
 * This extends the base storage class, adding required special handling for
 * Dialog entities.
 *
 * @ingroup guide
 */
interface DialogStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Dialog revision IDs for a specific Dialog.
   *
   * @param \Drupal\guide\Entity\DialogInterface $entity
   *   The Dialog entity.
   *
   * @return int[]
   *   Dialog revision IDs (in ascending order).
   */
  public function revisionIds(DialogInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Dialog author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Dialog revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\guide\Entity\DialogInterface $entity
   *   The Dialog entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DialogInterface $entity);

  /**
   * Unsets the language for all Dialog with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
