<?php

namespace Drupal\guide;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\guide\Entity\DialogInterface;

/**
 * Defines the storage handler class for Dialog entities.
 *
 * This extends the base storage class, adding required special handling for
 * Dialog entities.
 *
 * @ingroup guide
 */
class DialogStorage extends SqlContentEntityStorage implements DialogStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DialogInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {dialog_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {dialog_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DialogInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {dialog_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('dialog_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
