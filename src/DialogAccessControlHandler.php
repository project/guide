<?php

namespace Drupal\guide;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Dialog entity.
 *
 * @see \Drupal\guide\Entity\Dialog.
 */
class DialogAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\guide\Entity\DialogInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished dialog entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published dialog entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit dialog entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete dialog entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add dialog entities');
  }

}
