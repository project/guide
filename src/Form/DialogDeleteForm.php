<?php

namespace Drupal\guide\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Dialog entities.
 *
 * @ingroup guide
 */
class DialogDeleteForm extends ContentEntityDeleteForm {


}
