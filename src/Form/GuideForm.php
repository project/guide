<?php
/**
 * @file
 * Contains \Drupal\guide\Form\GuideForm.
 */

namespace Drupal\guide\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\guide\Entity\DialogInterface;
use Drupal\guide\Entity\Question;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contribute form.
 */
class GuideForm extends FormBase {

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * Constructs a \Drupal\guide\Form\GuideForm.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   * @param \Drupal\Core\Session\AccountInterface $current_user
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;

    $this->store = $this->tempStoreFactory->get('guide_data');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  /**
   * Saves the data from the guide form.
   */
  protected function saveData() {
    // Logic for saving data goes here...
    $this->deleteStore();
    //drupal_set_message($this->t('The form has been saved.'));

  }

  /**
   * Helper method that removes all the keys from the store collection used for
   * the guide form.
   */
  protected function deleteStore() {
    $keys = ['delta', 'decisions', 'filter'];
    foreach ($keys as $key) {
      $this->store->delete($key);
    }
  }



  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'guide_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DialogInterface $dialog = NULL) {

    // Start a manual session for anonymous users.
    if ($this->currentUser->isAnonymous() && !isset($_SESSION['guide_form_session'])) {
      $_SESSION['guide_form_session'] = true;
      $this->sessionManager->start();
      $this->store->set('delta', 0);
    }


    $form['intro'] = array(
      '#type' => 'markup',
      '#markup' => t('This is the start of a guide a.k.a. dialog.') . " Dialog ID" . $dialog->id(),
    );
    $conditions = [];
    foreach ($dialog->get('conditions') as $condition) {
      $values = explode('.', $condition->value);
      $conditions[$values[0]][$values[1]] = $values[2];
    }
    $decisions = $this->store->get('decisions') ? $this->store->get('decisions') : [];

    $current_delta = $this->store->get('delta') ? $this->store->get('delta') : 0;
    $question_ids = $dialog->get('questions')->getValue();
    $displayed_delta = NULL;
    foreach ($question_ids as $delta => $question_id) {
      $qid = $question_id["target_id"];
      if ($delta >= $current_delta) {
        if (array_key_exists($qid, $conditions)) {
          $show_question = FALSE;
          foreach ($conditions[$qid] as $q => $a) {
            if (array_key_exists("q_" . $q, $decisions) && $decisions["q_" . $q] == $a) {
              $show_question = TRUE;
              break;
            }
          }
          $current_delta++;
        }
        else {
          $show_question = TRUE;
        }
        if ($show_question) {
          $displayed_delta = $delta;
          $question = Question::load($qid);
          $answers = $question->get('answers')->getValue();
          $select_options = [];
          foreach ($answers as $answer) {
            $select_options[$answer['value']] = $answer['text'];
          }
//      $form['debug_q' . $question->id()] = array(
//        '#type' => 'markup',
//        '#markup' => "<pre>" . print_r($question->get('answers')->getValue(), 1) . "</pre>",
//      );
          $form['qid'] = [
            '#type' => 'hidden',
            '#value' => $question->id(),
          ];
          $form['question'] = [
            '#type' => 'radios',
            '#title' => strip_tags($question->get('text')->getValue()[0]['value']),
            '#options' => $select_options,
            '#required' => TRUE,
          ];
          break;
        }
      }
      #$current_delta++;
      $this->store->set('delta', $current_delta);
    }

    if (is_null($displayed_delta)) {
      $form['nomore'] = array(
        '#type' => 'markup',
        '#markup' => "<div>" . t('There are no more questions left. Click submit to see suggested results for your inquiry.') . "</div>",
      );
    }

    $form['debug'] = array(
      '#type' => 'markup',
      '#markup' => "<pre>Delta: " . print_r($current_delta, 1) . "\nDecisions: " . print_r(($this->store->get('decisions') ? $this->store->get('decisions') : []), 1) . "\nConditions: " . print_r($conditions, 1) . "\nFilter: " . print_r(($this->store->get('filter') ? $this->store->get('filter') : []), 1) . "</pre>",
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight' => 10,
    );

    if ($current_delta < max(array_keys($question_ids))) {
      $form['actions']['submit']['#value'] = $this->t('Next');
      $form['actions']['submit']['#name'] = 'next';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $current_delta = $this->store->get('delta') ? $this->store->get('delta') : 0;
    $this->store->set('delta', $current_delta + 1);

    $decisions = $this->store->get('decisions') ? $this->store->get('decisions') : [];
    $decisions["q_" . $values["qid"]] = $values["question"];
    $this->store->set('decisions', $decisions);

    $dialog = \Drupal::request()->attributes->get('dialog');
    $target = $dialog->get('target')->getValue()[0]['value'];

    /** @var \Drupal\facets\FacetManager\DefaultFacetManager $facet_manager */
    $facet_manager = \Drupal::service('facets.manager');

    $enabled_facets = $facet_manager->getEnabledFacets();
    $allowed_facets = [];
    foreach ($enabled_facets as $facet) {
      $facet_source_id = $facet->getFacetSourceId();
      \Drupal::logger('guide')->notice($facet_source_id . " <> " . $target);
      if ($facet_source_id == $target) {
        $allowed_facets[] = $facet;
      }
    }

    $filter = $this->store->get('filter') ? $this->store->get('filter') : [];
    if (array_key_exists('qid', $values)) {
      $question = Question::load($values["qid"]);
      $answers = $question->get('answers')->getValue();
      foreach ($answers as $answer) {
        if ($answer['value'] == $values["question"]) {
          $options = $answer['options'];
          foreach ($options['facets'] as $facetid) {
            foreach ($allowed_facets as $facet) {
              if ($facet->id() == $facetid) {
                $alias = $facet->getUrlAlias();
                $filter_values = $options['values']['facet-' . $facetid];
                foreach ($filter_values as $filter_value) {
                  $filter[] = $alias . ':' . $filter_value;
                }
              }
            }
          }
        }
      }
    }
    $this->store->set('filter', $filter);

    $button_clicked = $form_state->getTriggeringElement()['#name'];
    if ($button_clicked == 'submit') {
      $facet = $allowed_facets[0];
      $source = $facet->getFacetSource();

      $query = [];
      foreach ($filter as $key => $val) {
        $query['f[' . $key . ']'] = $val;
      }
      $url = Url::fromUserInput($source->getPath(), [
        'query' => $query,
      ]);
      $this->deleteStore();
      $form_state->setRedirectUrl($url);
    }
    else {
      $dialog = \Drupal::request()->attributes->get('dialog');
      $url = Url::fromRoute('guide.form')
        ->setRouteParameters(array('dialog' => $dialog->id()));
      $form_state->setRedirectUrl($url);
    }
  }
}
