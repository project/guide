<?php
/**
 * @file
 * Contains \Drupal\guide\Form\DialogLogicForm.
 */

namespace Drupal\guide\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\guide\Entity\Dialog;
use Drupal\guide\Entity\DialogInterface;
use Drupal\guide\Entity\Question;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contribute form.
 */
class DialogLogicForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dialog_logic_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DialogInterface $dialog = NULL) {

    $form['#tree'] = TRUE;
    $form['intro'] = array(
      '#type' => 'markup',
      '#markup' => $this->t('The logic for this dialog consists of conditions which determine if a question is going to be shown to the user based on her or his answers to previous questions.'),
    );
    $form['did'] = array(
      '#type' => 'hidden',
      '#value' => $dialog->id(),
    );

    #$questions = [];
    $questions = [0 => $this->t('- Please choose -')];
    $answers = [];
    $conditions = $dialog->get('conditions')->getValue();

    $question_ids = $dialog->get('questions')->getValue();
    foreach ($question_ids as $delta => $question_id) {
      // Get the question entity.
      $question = Question::load($question_id["target_id"]);
      $questions[$question->id()] = strip_tags($question->get('text')->getValue()[0]['value']);

      // Get the answers.
      $answers[$question->id()][0] = $this->t("- Please choose -");
      $answer_values = $question->get('answers')->getValue();
      $debug = [];
      foreach ($answer_values as $answer) {
        #$debug[] = $answer;
        $answers[$question->id()][$answer['value']] = $answer['text'];
      }
    }

    $form['conditions'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'conditions'],
    ];

    $condition_count = $form_state->get('condition_count');
    if (empty($condition_count) || $condition_count < count($conditions)) {
      $form_state->set('condition_count', count($conditions) + 1);
      $condition_count = $form_state->get('condition_count');
    }

    for ($c = 0; $c < $condition_count; $c++) {
      $delta = $c;
      $condition_exists = FALSE;
      $condition = NULL;
      $condition_values = [];
      if (array_key_exists($c, $conditions)) {
        $condition_exists = TRUE;
        $condition = $conditions[$c]['value'];
        $condition_values = explode('.', $condition);
      }

      $target_question = !is_null($form_state->getValues()['conditions'][$delta]['target_question']) ? $form_state->getValues()['conditions'][$delta]['target_question'] : $condition_values[0];
      $source_question = !is_null($form_state->getValues()['conditions'][$delta]['source_question']) ? $form_state->getValues()['conditions'][$delta]['source_question'] : $condition_values[1];
      $source_answer = !is_null($form_state->getValues()['conditions'][$delta]['source_answer']) ? $form_state->getValues()['conditions'][$delta]['source_answer'] : $condition_values[2];

      $form['conditions'][$delta] = array(
        '#type' => 'fieldset',
        '#title' => $this->t("Condition %condition", ["%condition" => $delta + 1]),
      );

      $debug = [
        'form_state' => [
          'target_question' => $form_state->getValues()['conditions'][$delta]['target_question'],
          'source_question' => $form_state->getValues()['conditions'][$delta]['source_question'],
          'source_answer' => $form_state->getValues()['conditions'][$delta]['source_answer'],
        ],
        'database' => $condition_values,
      ];

      $form['conditions'][$delta]['debug'] = array(
        '#type' => 'markup',
        '#markup' => "<pre>" . print_r($debug, 1) . "</pre>",
      );

      $form['conditions'][$delta]['target_question'] = [
        '#type' => 'select',
        '#title' => $this->t("The question"),
        '#options' => $questions,
        '#default_value' => $target_question,
      ];
      $form['conditions'][$delta]['source_question'] = [
        '#type' => 'select',
        '#title' => $this->t("shall only be shown if the question"),
        '#options' => $questions,
        '#ajax' => [
          'callback' => '::ajaxCallback',
          'wrapper' => 'conditions',
        ],
        '#default_value' => $source_question,
      ];
      if ($source_question) {
        $source_question_id = $form_state->getValues()['conditions'][$delta]['source_question'] > 0 ? $form_state->getValues()['conditions'][$delta]['source_question'] : $condition_values[2];
        $form['conditions'][$delta]['source_answer'] = [
          '#type' => 'select',
          '#title' => $this->t("was answered with"),
          '#options' => $answers[$source_question],
          '#default_value' => $source_answer,
        ];
      }

    }

    $form['conditions']['actions']['add_condition'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add condition'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'conditions',
      ],
    ];

    $form['conditions']['debug'] = array(
      '#type' => 'markup',
      '#markup' => "<pre>" . print_r($conditions, 1) . "</pre>",
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Save conditions'),
      '#button_type' => 'primary',
      '#weight' => 10,
    );

    return $form;
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['conditions'];
  }

  public function addOne(array &$form, FormStateInterface $form_state) {
    $conditions = $form_state->get('condition_count');
    $add_button = $conditions + 1;
    $form_state->set('condition_count', $add_button);
    $form_state->setRebuild();
  }

  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['conditions'];
  }

  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('conditions');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_names', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $button_clicked = $form_state->getTriggeringElement()['#name'];
    if ($button_clicked == 'submit') {
      $dialog = Dialog::load($values['did']);
      if ($dialog) {
        $condition_values = [];
        foreach ($values['conditions'] as $delta => $condition) {
          if ($condition['target_question'] > 0
            && $condition['source_question'] > 0
            && $condition['source_answer'] > 0) {
            $condition_values[] = $condition['target_question'] . '.' . $condition['source_question'] . '.' . $condition['source_answer'];
          }
        }
        $dialog->set('conditions', $condition_values);
        $dialog->save();
      }
    }
  }
}
?>
