<?php

namespace Drupal\guide\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Question entities.
 *
 * @ingroup guide
 */
class QuestionDeleteForm extends ContentEntityDeleteForm {


}
