<?php

/**
 * @file
 * Contains dialog.page.inc.
 *
 * Page callback for Dialog entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Dialog templates.
 *
 * Default template: dialog.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_dialog(array &$variables) {
  // Fetch Dialog Entity Object.
  $dialog = $variables['elements']['#dialog'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
